module.exports = {
	'{src/README.md,CHANGELOG.md,config.example.json,package.json}': [
		() => 'repo-utils/mdFileInclude.cjs src/README.md README.md',
		'git add README.md'
	],
	'index.js': ['npm run typings', 'git add index.d.ts'],
	'*.{css,md,json}': ['prettier --write'],
	'*.{cjs,js,svelte,ts}': ['prettier --write', 'eslint -c .eslintrc.commit.cjs --fix']
};
