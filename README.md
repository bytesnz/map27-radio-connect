# @on-location/map27-radio-connect 0.2.1

MAP27 radio interface module for on-location

[![pipeline status](https://gitlab.com/bytesnz//badges/main/pipeline.svg)](https://gitlab.com/bytesnz/map27-radio-connect/-/commits/main)
[![license](https://bytes.nz/b//custom?color=yellow&name=license&value=AGPL-3.0)](https://gitlab.com/bytesnz/map27-radio-connect/-/blob/main/LICENSE)
[![developtment time](https://bytes.nz/b/map27-radio-connect/custom?color=yellowgreen&name=development+time&value=~3+hours)](https://gitlab.com/bytesnz/map27-radio-connect/-/blob/main/.tickings)
[![contributor covenant](https://bytes.nz/b//custom?color=purple&name=contributor+covenant&value=v1.4.1+adopted)](https://gitlab.com/bytesnz/map27-radio-connect/-/blob/main/CODE_OF_CONDUCT.md)
[![support development](https://bytes.nz/b//custom?color=brightgreen&name=support+development&value=$$)](https://liberapay.com/MeldCE)

Node server to log radio events, received positions and poll radios through
on-location.

## Ongoing Development

**This package is currently in active development and some features may
not yet be implemented**. Please check out the
[issue tracker](https://gitlab.com/bytesnz/map27-radio-connect/issues) for information on the development.

The list of current features implemented and still to be implemented is:

- [x] Fetching radios from on-location
- [x] Parsing and submitting positions from NMEA polls for DMR radios
- [ ] Parsing and submitting positions from NMEA polls for non-DMR radios (#1)
- [x] Polling DMR radios for their position (#2)
- [ ] Polling non-DMR radios for their position (#3)
- [x] Handling and submitting errors during polling to on-location (#1)
- [ ] Handling and submitting call logs to on-location (#4)

## Usage

Create a configuration file for the on-location web socket connection and
the connected radios.

```json
{
  "server": "ws://localhost:3001",
  "logLevel": "INFO",
  "ports": [
    {
      "port": "COM1",
      "baud": 19200,
      "networkType": "dmr",
      "protocol": "taitAscii",
      "use": ["dmrPolling"]
    },
    {
      "port": "STDIN",
      "baud": 19200,
      "protocol": "taitAscii",
      "networkType": "dmr",
      "use": ["dmrPolling"]
    }
  ]
}

```

Run the service

```shell
npx on-radio-connect
```

It can also be used as a node module

```js
import startRadioService from '@on-location/map27-radio-connect';

const config = {
  server: 'wss://example.com',
  ports: [
    {
      ...
    }
  ]
};

try {
  startRadioService(config);
} catch (error) {
  console.error(error);
}
```

## Development

Feel free to post errors or feature requests to the project
[issue tracker](https://gitlab.com/bytesnz/map27-radio-connect/issues) or
[email](mailto:contact-project+bytesnz-map27-radio-connect-38261806-issue-@incoming.gitlab.com) them to us.
**Please submit security concerns as a
[confidential issue](https://gitlab.com/bytesnz/map27-radio-connect/issues?issue[confidential]=true)**

The source is hosted on [Gitlab](https://gitlab.com/bytesnz/map27-radio-connect) and uses
[eslint][], [prettier][], [lint-staged][] and [husky][] to keep things pretty.
As such, when you first [clone][git-clone] the repository, as well as
installing the npm dependencies, you will also need to install [husky][].

```bash
# Install NPM dependencies
npm install
# Set up husky Git hooks stored in .husky
npx husky install
```

## Mocking Radios

For development, Tait DMR radios can be mocked using the
_test/taitDmrRadioMock.js_ script.

1. On Windows, create a virtual serial loopback pair, with something like
   [com0com][].\
   On Linux, use `socat` to create a virtual serial loopback pair
   ```shell
   socat PTY,link=/dev/ttyS10 PTY,link=/dev/ttyS11
   ```
1. Run _test/taitDmrRadioMock.js_ connecting it to one of the serial interfaces
   ```shell
   node test/taitDmrRadioMock.js -p /dev/ttyS11 -b 19200 -a
   ```
   (the `-a` flag will set the mocker to automatically reply to serial
   messages)
1. Configure @on-location/map27-radio-connect using a _config.json_ file to connect to
   the other serial interface
1. Run @on-location/map27-radio-connect
   ```shell
   node bin/run.js
   ```

# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [0.2.1] - 2023-05-02

### Added

- Basic authentication configuration options for connecting to on-location
  server

## [0.2.0] - 2023-03-23

### Added

- Publishing of MAP27 service to on-location and responding to location poll
  service requests
- Handling of more status messages to ensure the NACKs and queued ACKs are
  handled, and information requests are sent to the radio to ensure it is
  online

## [0.1.0] - 2022-06-15

Initial version! Implemented features are:

- Fetching radios from on-location
- Parsing and submitting positions from NMEA polls for DMR radios

[0.2.1]: https://gitlab.com/bytesnz/map27-radio-connect/-/compare/v0.2.0...v0.2.1
[0.2.0]: https://gitlab.com/bytesnz/map27-radio-connect/-/compare/v0.1.0...v0.2.0
[0.1.0]: https://gitlab.com/bytesnz/map27-radio-connect/-/tree/v0.1.0


[husky]: https://typicode.github.io/husky
[eslint]: https://eslint.org/
[git-clone]: https://www.git-scm.com/docs/git-clone
[prettier]: https://prettier.io/
[lint-staged]: https://github.com/okonet/lint-staged#readme
[gitlab-ci]: https://docs.gitlab.com/ee/ci/
[com0com]: https://com0com.sourceforge.net/
