# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [0.2.1] - 2023-05-02

### Added

- Basic authentication configuration options for connecting to on-location
  server

## [0.2.0] - 2023-03-23

### Added

- Publishing of MAP27 service to on-location and responding to location poll
  service requests
- Handling of more status messages to ensure the NACKs and queued ACKs are
  handled, and information requests are sent to the radio to ensure it is
  online

## [0.1.0] - 2022-06-15

Initial version! Implemented features are:

- Fetching radios from on-location
- Parsing and submitting positions from NMEA polls for DMR radios

[0.2.1]: https://gitlab.com/bytesnz/map27-radio-connect/-/compare/v0.2.0...v0.2.1
[0.2.0]: https://gitlab.com/bytesnz/map27-radio-connect/-/compare/v0.1.0...v0.2.0
[0.1.0]: https://gitlab.com/bytesnz/map27-radio-connect/-/tree/v0.1.0
