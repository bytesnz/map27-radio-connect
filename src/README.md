# <!=package.json name> <!=package.json version>

<!=package.json description>

[![pipeline status](https://gitlab.com/bytesnz/<!=package.json name \/(._)$>/badges/main/pipeline.svg)](<!=package.json repository.url>/-/commits/main)
[![license](https://bytes.nz/b/<!=package.json name \/(._)$>/custom?color=yellow&name=license&value=AGPL-3.0)](<!=package.json repository.url>/-/blob/main/LICENSE)
[![developtment time](https://bytes.nz/b/<!=package.json name \/(.*)$>/custom?color=yellowgreen&name=development+time&value=~3+hours)](<!=package.json repository.url>/-/blob/main/.tickings)
[![contributor covenant](https://bytes.nz/b/<!=package.json name \/(._)$>/custom?color=purple&name=contributor+covenant&value=v1.4.1+adopted)](<!=package.json repository.url>/-/blob/main/CODE_OF_CONDUCT.md)
[![support development](https://bytes.nz/b/<!=package.json name \/(._)$>/custom?color=brightgreen&name=support+development&value=$$)](https://liberapay.com/MeldCE)

Node server to log radio events, received positions and poll radios through
on-location.

## Ongoing Development

**This package is currently in active development and some features may
not yet be implemented**. Please check out the
[issue tracker](<!=package.json bugs.url>) for information on the development.

The list of current features implemented and still to be implemented is:

- [x] Fetching radios from on-location
- [x] Parsing and submitting positions from NMEA polls for DMR radios
- [ ] Parsing and submitting positions from NMEA polls for non-DMR radios (#1)
- [x] Polling DMR radios for their position (#2)
- [ ] Polling non-DMR radios for their position (#3)
- [x] Handling and submitting errors during polling to on-location (#1)
- [ ] Handling and submitting call logs to on-location (#4)

## Usage

Create a configuration file for the on-location web socket connection and
the connected radios.

```json
<!=config.example.json>
```

Run the service

```shell
npx on-radio-connect
```

It can also be used as a node module

```js
import startRadioService from '@on-location/map27-radio-connect';

const config = {
  server: 'wss://example.com',
  ports: [
    {
      ...
    }
  ]
};

try {
  startRadioService(config);
} catch (error) {
  console.error(error);
}
```

## Development

Feel free to post errors or feature requests to the project
[issue tracker](<!=package.json bugs.url>) or
[email](mailto:<!=package.json bugs.email>) them to us.
**Please submit security concerns as a
[confidential issue](<!=package.json bugs.url>?issue[confidential]=true)**

The source is hosted on [Gitlab](<!=package.json repository.url>) and uses
[eslint][], [prettier][], [lint-staged][] and [husky][] to keep things pretty.
As such, when you first [clone][git-clone] the repository, as well as
installing the npm dependencies, you will also need to install [husky][].

```bash
# Install NPM dependencies
npm install
# Set up husky Git hooks stored in .husky
npx husky install
```

## Mocking Radios

For development, Tait DMR radios can be mocked using the
_test/taitDmrRadioMock.js_ script.

1. On Windows, create a virtual serial loopback pair, with something like
   [com0com][].\
   On Linux, use `socat` to create a virtual serial loopback pair
   ```shell
   socat PTY,link=/dev/ttyS10 PTY,link=/dev/ttyS11
   ```
1. Run _test/taitDmrRadioMock.js_ connecting it to one of the serial interfaces
   ```shell
   node test/taitDmrRadioMock.js -p /dev/ttyS11 -b 19200 -a
   ```
   (the `-a` flag will set the mocker to automatically reply to serial
   messages)
1. Configure <!=package.json name> using a _config.json_ file to connect to
   the other serial interface
1. Run <!=package.json name>
   ```shell
   node bin/run.js
   ```

<!=CHANGELOG.md>

[husky]: https://typicode.github.io/husky
[eslint]: https://eslint.org/
[git-clone]: https://www.git-scm.com/docs/git-clone
[prettier]: https://prettier.io/
[lint-staged]: https://github.com/okonet/lint-staged#readme
[gitlab-ci]: https://docs.gitlab.com/ee/ci/
[com0com]: https://com0com.sourceforge.net/
