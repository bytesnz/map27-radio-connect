#!/bin/env node
import { exit } from 'node:process';
import { readFile } from 'node:fs/promises';
import startRadioConnect from '../index.js';
import Logger from '../lib/log.js';

const logger = Logger('map27-radio-connect')
/// @type {import('../index.js').Config}
let config = {
	logger
};

if (process.env.LOG_LEVEL) {
  logger.log('Changing log level to ', process.env.LOG_LEVEL);
  logger.setLogLevel(process.env.LOG_LEVEL);
}

try {
	const configFile = JSON.parse(await readFile('./config.json'));

	Object.assign(config, configFile);
} catch {}

const L = config.logger;

try {
	startRadioConnect(config);
} catch (error) {
	L.error(error.message);
  console.error(error);
	exit(1);
}
