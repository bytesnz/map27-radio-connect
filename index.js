import { stdin } from 'node:process';
import { randomUUID } from 'node:crypto';
import { readFile, writeFile } from 'node:fs/promises';
import { SerialPort } from 'serialport';
import * as Map27 from 'map27';
import WebSocket from 'ws';

/** @typedef {object} RequestData
 *
 * @prop {"locationPoll"} type Type of request
 * @prop {string[]} ids on-location IDs of radios to poll
 * @prop {string} radioId on-location ID of radio to use for poll
 */

/** @typedef {object} Request
 *
 * @prop {number} requestId Request socket message id
 * @prop {RequestData} request Data for request
 * @prop {number} [idIndex] Current index of radio in ids list being polled
 * @prop {number} [attempt] Current attempt to poll current radio
 */

/** @typedef {object} PortRadio
 * Details of serial port radio
 *
 * @prop {string} id On-location id string of radio
 * @prop {string} [dmrId] DMR ID of radio
 * @prop {'available'|'unavailable'|'offline'|'removed'} status Status or radio
 * @prop {'transmitting'|'oncall'|'ready'|'no_reception'} state State of radio
 */

/**
 * Start radio connect service
 *
 * @param {import('./config.d.ts').Config} config Configuratio to use
 *
 * @throws {Error} On fatal error or on serial port error
 */
async function startRadioConnect(config) {
	let messageId = 1;

	/// Radios from on-location keyed on on-location ID
	let radios = {};

	/// Radios from on-location key on ID
	let radioMap = {};

	let serverId = null;
	let radioServiceId = 'map-connect';
	let radioServiceStatus = 'available';

	let wsMessageQueue = [];
	let socketPing = null;
	let ws = null;
	/// Serial port connection for the port address
	const serialPorts = {};
	/// Map of port configs
	const portConfigs = {};
	/** @type {{ [port: string]: Logger }}
	 * Logger for serial ports
	 */
	const portLogger = {};
	/// Map of port addresses for port to forward data to
	const forwards = {};
	/// Timeout to wait inbetween WS connection attempts
	let wsErrorTimeout = 1000;
	let wsTimeout = null;
	/** @type {{ [port: string]: PortRadio }}
	 * Serial port radio information keyed on the serial port
	 */
	const portRadio = {};
	/** @type {{ [port: string]: Request[] }}
	 * Queue of requests to complete on radio keyed on the serial port
	 */
	const portQueue = {};
	/** @type {{ [port: string]: number }}
	 *
	 * Stores timeout for checking response from messages from queue
	 */
	const portTimeouts = {};
	/** @type {{ [port: string]: number }}
	 *
	 * Stores the timecode the last message was received on the serial port
	 */
	const lastPortMessage = {};

	const radioType = (radioData) => findFirstMatch(radioData.type, ['dmr-radio']);

	setInterval(() => {
		const now = new Date().getTime();
		const nowOffline = [];
		for (const port of Object.keys(portTimeouts)) {
			if (portRadio[port].status !== 'offline' && now - portTimeouts[port] > 60000) {
				portRadio[port].status = 'offline';
				nowOffline.push(port);
			}
		}
		if (nowOffline.length) {
			publishStatusChange(nowOffline);
		}
	}, 10000);

	/**
	 * Connect to the on-location web socket and get the latest list of
	 * radios
	 */
	const connectWs = () => {
		const wsOptions = {};

		if (config.username && config.password) {
			wsOptions.auth = `${config.username}:${config.password}`;
		}

		L.log(`Connecting to web socket ${config.server}`);
		ws = new WebSocket(config.server, 'on-location', wsOptions);

		ws.on('open', () => {
			wsErrorTimeout = 1000;
			L.log(`Connected to ${config.server}`);

			// Subscribe for changes in radios
			ws.send(
				JSON.stringify({
					t: 'assets:subscribe',
					i: messageId++,
					d: {
						types: ['radio'],
						callsigns: true,
						useId: true
					}
				})
			);

			socketPing = setInterval(sendUpdate, 30000);

			if (wsMessageQueue) {
				L.debug('Sending queued web socket messages');
				for (let i = 0; i < wsMessageQueue.length; i++) {
					ws.send(wsMessageQueue[i]);
				}
				wsMessageQueue = [];
			}

			publishService();
		});

		ws.on('message', (data) => {
			try {
				data = JSON.parse(data);
				L.debug('Received socket message', data);
			} catch (error) {
				L.warn(`Received socket message that could not parse: ${data}`);
				return;
			}

			switch (data.t) {
				case 'assets:list':
					radioMap = {};
					radios = {};
					for (let i = 0; i < data.d.length; i++) {
						switch (radioType(data.d[i])) {
							case 'dmr-radio':
								radioMap[radioId(data.d[i])] = data.d[i];
								break;
							default:
								L.warn('Unknown radio type for', data.d[i].name, data.d[i].type);
						}
						radios[data.d[i].id] = data.d[i];
					}
					L.log('Received', data.d.length, 'radio assets');
					L.debug('radioMap list replaced', radioMap);
					break;
				case 'assets:update':
					radioMap[radioId(data.d)] = data.d;
					radios[data.d.id] = data.d;
					break;
				case 'services:service:request':
					switch (data.d.type) {
						case 'locationPoll': {
							L.info('Got location poll request for', data.d.ids);
							// Check radioId
							let pollRadio;
							let port;
							if (
								!data.d.radioId ||
								!(pollRadio = radios[data.d.radioId]) ||
								!(port = findRadioPort(pollRadio.id))
							) {
								L.debug('Unknown radio ID given', data.d.radioId);
								sendSocketMessage(
									JSON.stringify({
										t: 'services:service:request:error',
										i: data.i,
										d: {
											message: 'Invalid radio'
										}
									})
								);
								break;
							}

							// TODO Check can be used for poll
							L.debug('Got radio for poll', pollRadio);

							// Check have details for radio
							const unknown = [];
							for (const id of data.d.ids) {
								const radio = radios[id];
								L.debug('checking radio', id, radio);

								if (!radio || radio.prefix !== pollRadio.prefix) {
									L.debug('Radio', id, 'unknown or does not have same prefix as', pollRadio.id);
									unknown.push(id);
									continue;
								}
							}

							if (unknown.length) {
								sendSocketMessage(
									JSON.stringify({
										t: 'services:service:request:error',
										i: data.i,
										d: {
											message: 'Invalid radio',
											ids: unknown
										}
									})
								);
								break;
							}

							addRequest(port, {
								requestId: data.i,
								request: data.d
							});

							sendSocketMessage(
								JSON.stringify({
									t: 'services:service:request:ack',
									i: data.i
								})
							);
							break;
						}
						default:
							sendSocketMessage(
								JSON.stringify({
									t: 'services:service:request:error',
									i: data.i,
									d: {
										message: 'Unknown service request ' + data.d.type
									}
								})
							);
							break;
					}
					break;
				default:
					L.log('Received unhandled socket message', JSON.stringify(data, null, 2));
			}
		});

		ws.on('error', (error) => {
			L.error('Error on web socket', error.message);

			ws = null;
			wsErrorTimeout = Math.min(wsErrorTimeout * 2, 16000);

			if (wsTimeout === null) {
				wsTimeout = setTimeout(() => {
					connectWs();
					wsTimeout = null;
				}, wsErrorTimeout);
			}
		});

		ws.on('close', () => {
			L.log('Socket closed.');
			clearInterval(socketPing);
			if (wsTimeout === null) {
				wsTimeout = setTimeout(() => {
					connectWs();
					wsTimeout = null;
				}, wsErrorTimeout);
			}
		});
	};

	/**
	 * Publish information about the services to the web socket
	 */
	function publishService() {
		const radioService = {
			type: 'map27',
			id: radioServiceId,
			status: radioServiceStatus,
			radios: []
		};
		const data = {
			guid: serverId,
			services: [radioService]
		};

		for (let port in portRadio) {
			// Find radio in radios from on-location server
			if (radioMap[portRadio[port].mapId]) {
				radioService.radios.push({
					id: radioMap[portRadio[port].mapId].id,
					type: 'dmr-radio',
					status: portRadio[port].status,
					use: portConfigs[port].use
				});
			}
		}

		L.info('publishing services');
		L.debug(JSON.stringify(data, null, 2));
		sendSocketMessage(
			JSON.stringify({
				t: 'services:service:connect',
				i: messageId++,
				d: data
			})
		);
	}

	/**
	 * Send web socket message with change in radio status
	 *
	 * @param {string[]} port Port to send status update for
	 */
	function publishStatusChange(ports) {
		L.info('publishing radio status update for', ports);
		const radios = ports.map((port) => ({
			id: radioMap[portRadio[port].mapId].id,
			status: portRadio[port].status
		}));
		sendSocketMessage(
			JSON.stringify({
				t: 'services:service:update',
				i: messageId++,
				d: {
					guid: serverId,
					services: [
						{
							id: radioServiceId,
							status: radioServiceStatus,
							radios
						}
					]
				}
			})
		);
	}

	/**
	 * Send message to web socket keep alive
	 */
	function sendUpdate() {
		const radios = [];

		for (const radio of Object.values(portRadio)) {
			radios.push({
				id: radio.id,
				status: radio.status
			});
		}

		if (radios.length) {
			L.info('Sending status');
			const data = {
				guid: serverId,
				services: [
					{
						id: radioServiceId,
						status: radioServiceStatus,
						radios
					}
				]
			};
			L.debug(JSON.stringify(data, null, 2));
			sendSocketMessage(
				JSON.stringify({
					t: 'services:service:update',
					i: messageId++,
					d: data
				})
			);
		} else {
			L.debug('Sending socket ping');
			ws.ping();
		}
	}

	/**
	 * Create radio ID string for radio in given message
	 *
	 * @param {object} message Message about radio
	 *
	 * @returns {string} Radio ID string
	 */
	function radioId(message) {
		let ident = message.IDENT1 || message.ident;
		const pfix = message.PFIX1 || message.prefix;
		if (!pfix || !ident) {
			throw new Error('Message not an addressed message');
		}

		ident = ident.toString();

		return pfix + '0'.repeat(4 - ident.length) + ident;
	}

	/**
	 * Find the DMR radio referenced in the given message
	 *
	 * @param {object} message MAP27 message with an address
	 *
	 * @returns {object|null} Found radio or null if no radio found
	 *
	 * @throws {Error} If the message is not addressed
	 */
	const findDmrRadio = (message) => {
		return radioMap[radioId(message)] || null;
	};

	/**
	 * Find the port the radio with the given id is on
	 */
	function findRadioPort(id) {
		for (const port in portRadio) {
			if (portRadio[port].id === id) {
				return port;
			}
		}
		return null;
	}

	/**
	 * Send a web socket message to on-location, or buffer it if the socket
	 * connection is down
	 *
	 * @param {string} message Message to send
	 */
	const sendSocketMessage = (message) => {
		if (!ws || ws.readyState === ws.OPEN) {
			ws.send(message);
		} else {
			wsMessageQueue.push(message);
		}
	};

	/**
	 * Find the first value in the haystack array that matches the values in the
	 * needles array
	 *
	 * @param {any[]} haystack Array to search
	 * @param {any[]} needles Array of values to find
	 *
	 * @returns {any|null} First found value, or null if no matching value found
	 */
	const findFirstMatch = (haystack, needles) => {
		for (let i = 0; i < haystack.length; i++) {
			if (needles.indexOf(haystack[i]) !== -1) {
				return haystack[i];
			}
		}

		return null;
	};

	/**
	 * Add request to request queue for serial port
	 *
	 * @param {string} port Port to add request to
	 * @param {Request} request Request
	 */
	function addRequest(port, request) {
		L.debug('Adding request');
		if (!portQueue[port]) {
			portQueue[port] = [request];
		} else {
			portQueue[port].push(request);

			if (portQueue[port].length > 1) {
				return;
			}
		}

		handlePortQueue(port);
	}

	/**
	 * Run queue for the serial port queue
	 *
	 * @param {string} port Port
	 */
	function handlePortQueue(port) {
		const portL = portLogger[port];
		portL.info('Running handlePortQueue');
		if (portTimeouts[port]) {
			clearTimeout(portTimeouts[port]);
		}
		// Check the current radio state
		if (portRadio[port].state !== 'ready' || !portQueue[port]?.length) {
			portL.debug('Not ready for run', portRadio[port].state, portQueue[port]?.length);
			return;
		}

		portL.info('Running queue');

		const item = portQueue[port][0];
		const { request, requestId } = item;

		portL.debug('Running item', item);

		// Start sending request
		switch (request.type) {
			case 'locationPoll': {
				// TODO Handle other radio types
				if (typeof item.idIndex === 'undefined') {
					item.idIndex = 0;
					item.attempt = 0;
				}

				// Check if finished with ids
				if (item.idIndex >= request.ids.length) {
					portL.info('Finished with request', requestId);
					// Send success message
					sendSocketMessage(
						JSON.stringify({
							t: 'services:service:request:update',
							i: requestId,
							d: {
								status: 'complete'
							}
						})
					);
					portQueue[port].shift();
					handlePortQueue(port);
					return;
				}

				// Generate the packet to send to radio
				let id = request.ids[item.idIndex];

				// TODO Change with have sorted 0xb5 message
				// If state is ready and try is !0, the last request must have
				// failed, so either retry or send an error
				if (item.attempt !== 0) {
					if (!request.retries || item.attempt >= request.retries) {
						// Send fail message
						sendSocketMessage(
							JSON.stringify({
								t: 'services:service:request:update',
								i: requestId,
								d: {
									status: 'failed',
									data: {
										radioId: id
									}
								}
							})
						);
						portTimeouts[port] = setTimeout(() => handlePortQueue(port), 10000);

						item.idIndex++;

						if (item.idIndex >= request.ids.length) {
							portQueue[port].shift();
							handlePortQueue(port);
						}

						id = request.ids[item.idIndex];
						item.attempt = 0;
					}
				}

				// Loop incase of unknown radios
				while (id) {
					// Ensure have details of radio
					if (!radios[id]) {
						// Send fail message
						sendSocketMessage(
							JSON.stringify({
								t: 'services:service:request:error',
								i: requestId,
								d: {
									status: 'failed',
									message: 'Unknown radio received',
									data: {
										radioId: id
									}
								}
							})
						);

						item.idIndex++;

						if (item.idIndex >= request.ids.length) {
							portQueue[port].shift();
							if (portQueue[port].length) {
								handlePortQueue(port);
							}
							return;
						}

						id = request.ids[item.idIndex];
						item.attempt = 0;
						continue;
					}

					// Generate binary message to send to radio
					// TODO Change to binary-decoder when ready
					const bytes = [0x84];

					// Convert id
					bytes.push(radios[id].prefix & 0x7f);
					bytes.push((radios[id].ident >> 8) & 0x1f);
					bytes.push(radios[id].ident & 0xff);

					// No addressing or length
					bytes.push(0x00);
					// Poll format NMEA
					bytes.push(5);

					item.attempt++;
					portRadio[port].state = 'commanded';
					// TODO Add a timeout so don't end up in commanded state forever

					// Convert to ASCII and send
					portLogger[port].debug('Sending message', Buffer.from(bytes).toString('hex'));
					serialPorts[port].write(Buffer.from(bytes).toString('hex') + '\r');

					break;
				}

				break;
			}
		}
	}

	/**
	 * Handle a decoded MAP27 message
	 *
	 * @param {object} message MAP27 message
	 * @param {object} portConfig Port configuration
	 */
	const handleRadioMessage = (message, portConfig) => {
		lastPortMessage[portConfig.port] = new Date().getTime();
		const portL = portLogger[portConfig.port];
		switch (message.messageType.value ?? message.messageType) {
			case 0x81: // SST message
				switch (message.CODING.value ?? message.CODING) {
					case 3: {
						// NMEA
						if (!(message.Quality.value ?? message.Quality)) {
							L.debug('Invalid position ignored');
							return;
						}
						let radio;
						// Find radio
						if (portConfig.networkType === 'dmr') {
							radio = findDmrRadio(message);
							if (!radio) {
								L.info(`Message from unknown DMR radio ${message.dmrId}`);
								return;
							}
							if (!radio?.callsigns?.[0]?.id) {
								L.warn(`DMR radio ${message.dmrId} has no associated callsign`);
							}
						} else {
							L.warn('Unknown networkType', portConfig.networkType);
							return;
						}

						const id = messageId++;

						// Convert units to degrees
						let latitude =
							message.latDegrees + message.latMinutes / 60 + message.latFraction / 60 / 10000;
						if (!(message.northSouth.value ?? message.northSouth)) {
							latitude = -latitude;
						}
						let longitude =
							message.longDegrees + message.longMinutes / 60 + message.longFraction / 60 / 10000;
						if (!(message.EastWest.value ?? message.EastWest)) {
							longitude = -longitude;
						}
						const time = new Date();
						// TODO Use utcHours utcMinutes utcSeconds

						const position = {
							source: radio.id,
							subject: radio.callsigns?.[0]?.id || radio.id,
							latitude,
							longitude,
							time
						};
						L.debug('Creating position', position);

						sendSocketMessage(
							JSON.stringify({
								t: 'positions:create',
								i: id,
								d: position
							})
						);

						// Check if the position is in the queue
						if (portQueue[portConfig.port]?.length) {
							portL.info('Checking port queue for radio poll');
							portL.debug('Port queue', JSON.stringify(portQueue[portConfig.port], null, 2));
							let i = 0;

							while (i < portQueue[portConfig.port].length) {
								const item = portQueue[portConfig.port][i];
								const { request, requestId } = item;

								if (request.type !== 'locationPoll') {
									i++;
									continue;
								}

								const index = request.ids.indexOf(radio.id);

								if (index === -1) {
									i++;
									continue;
								}

								portL.debug('Found radio in queue', i, index);
								if (index === item.idIndex) {
									item.attempt = 0;
									item.idIndex++;
								} else if (index > item.idIndex) {
									// Remove id from list of ids yet to be polled
									request.ids.splice(index, 1);
								}

								// Send request update
								sendSocketMessage(
									JSON.stringify({
										t: 'services:service:request:update',
										i: requestId,
										d: {
											status: 'progress',
											data: {
												radioId: radio.id
											}
										}
									})
								);

								i++;
							}
						}

						break;
					}
					default:
						L.info(`Unhandled CODING value ${message.CODING}`);
				}
				break;
			case 0xb0: {
				// Radio Personality
				L.info(
					`Got radio information on port ${portConfig.port}: Serial: ${message.serialNumber} DMR ID: ${message.dmrId}`
				);
				// Store radio information
				const radioInfo = {
					mapId: radioId(message),
					prefix: message.PFIX1,
					ident: message.IDENT1,
					manufacturer: message.manufacturerCode,
					model: message.model,
					serial: message.serialNumber,
					capabilities: message.capabilities,
					status: 'offline'
				};

				if (radioMap[radioInfo.mapId]) {
					radioInfo.id = radioMap[radioInfo.mapId].id;
				}

				// Check if same as existing (constructed the same so can used ===)
				if (
					!portRadio[portConfig.port] ||
					JSON.stringify(portRadio[portConfig.port]) !== JSON.stringify(radioInfo)
				) {
					portRadio[portConfig.port] = radioInfo;
					L.debug('Saving radio information for port', portConfig.port, portRadio[portConfig.port]);

					// Send service update
					publishService();
				}
				break;
			}
			case 0xb5: {
				// Network information
				if (!portRadio[portConfig.port] || portRadio[portConfig.port].status === 'offline') {
					if (!Object.keys(radios).length) {
						break;
					}

					// Get radio on port
					// Send interrogation to radio
					// TODO Change to use map27-decoder
					portL.debug('Sending b000 (personality)');
					portL.debug('Sending b003 (operating condition)');
					serialPorts[portConfig.port].write('b000\r'); // Personality request
					serialPorts[portConfig.port].write('b003\r'); // Operating condition
					break;
				}
				break;
			}
			case 0xb2: {
				// Operating Condition
				if (!portRadio[portConfig.port]) {
					if (!Object.keys(radios).length) {
						break;
					}

					// Get radio on port
					// Send interrogation to radio
					// TODO Change to use map27-decoder
					portL.debug('Sending b000 (personality)');
					portL.debug('Sending b003 (operating condition)');
					serialPorts[portConfig.port].write('b000\r'); // Personality request
					serialPorts[portConfig.port].write('b003\r'); // Operating condition
					break;
				}

				let state;
				let status = 'available';
				if (message.transmit.value) {
					state = 'transmitting';
				} else if (message.offHook.value) {
					state = 'oncall';
				} else if (message.fieldStrength === 0) {
					state = 'no_reception';
					status = 'unavailable';
				} else if (portRadio[portConfig.port].state !== 'commanded') {
					state = 'ready';
				}

				let statusChanged = portRadio[portConfig.port].status !== status;
				portRadio[portConfig.port].status = status;
				portRadio[portConfig.port].state = state;

				if (statusChanged) {
					publishStatusChange([portConfig.port]);
				}

				if (state === 'ready' && portQueue[portConfig.port]?.length) {
					portL.debug('Radio ready to run queue');
					handlePortQueue(portConfig.port);
				}
				break;
			}
			case 0xd0: // Message Queued ACK
			case 0xe0: // NACK
				if (portQueue[portConfig.port]?.length) {
					// Check if the first in queue for the same address
					const item = portQueue[portConfig.port][0];

					if (typeof item.idIndex === 'undefined') {
						break;
					}
					const id = item.request.ids[item.idIndex];
					const radio = radios[id];

					if (radio.prefix === message.PFIX1 && radio.ident === message.IDENT1) {
						switch (message.messageType.value ?? message.messageType) {
							case 0xd0:
								// Send an update
								sendSocketMessage(
									JSON.stringify({
										t: 'services:service:request:update',
										i: item.requestId,
										d: {
											status: 'queued',
											message: message.cause?.label || null,
											data: {
												radioId: id
											}
										}
									})
								);
								break;
							case 0xe0:
								// Continue queue (will send error if needed)
								handlePortQueue(portConfig.port);
								break;
						}
					}
				}
				break;
			default:
				L.info(`Unhandled ${message.messageType.label ?? message.messageType} message`);
		}
	};

	const L = config.logger;

	if (config.logLevel) {
		L.setLogLevel(config.logLevel);
	}

	if (!config.ports?.length) {
		throw new Error('No ports specified');
	}

	if (!config.server) {
		throw new Error('No on-location server specified');
	}

	try {
		serverId = (await readFile('.serverid'))?.toString();
	} catch {
		void 1;
	}

	if (!serverId) {
		serverId = randomUUID();
		await writeFile('.serverid', serverId);
	}

	// Connect to web socket
	connectWs();

	let stdinUsed = false;
	// Set up listeners
	for (let i = 0; i < config.ports.length; i++) {
		const portConfig = config.ports[i];
		portConfigs[portConfig.port] = portConfig;

		if (!portConfig.port) {
			throw new Error(`No port specified for port ${i + 1}`);
		}

		let port;
		if (portConfig.port === 'STDIN') {
			if (stdinUsed) {
				throw new Error('Cannot have multiple radios connected to STDIN');
			}

			stdinUsed = true;
			port = stdin;
		} else {
			port = new SerialPort({
				path: portConfig.port,
				baudRate: portConfig.baud || 19200
			});
			serialPorts[portConfig.port] = port;

			port.on('error', (error) => {
				throw new Error(`Error on port ${portConfig.port}: ${error.message}`);
			});
		}

		switch (portConfig.protocol) {
			case 'taitAscii': {
				const portL = L.createLogger(':' + portConfig.port);
				portLogger[portConfig.port] = portL;
				const coder = Map27.createTaitAsciiCoder({
					logger: portL.createLogger(':taitAsciiDecoder', 'INFO')
				});
				portL.debug(`Added decoder for ${portConfig.port}`);

				let buffer = [];

				port.on('data', (data) => {
					portL.debug('got data', data);
					let decodedMessage;
					for (let i = 0; i < data.length; i++) {
						if (data[i] == 0x0d) {
							// \r
							// decode
							if (!buffer.length) {
								continue;
							}
							try {
								if (portL.willLog('INFO')) {
									portL.info(
										'Received message',
										buffer.reduce((s, c) => s + String.fromCharCode(c), '')
									);
								}
								({ decodedMessage } = coder.decode(new Uint8Array(buffer)));
								portL.debug('Got message', decodedMessage);

								handleRadioMessage(decodedMessage, portConfig);

								// Forward message to program ports
								if (forwards[portConfig.port]) {
									for (let forwardPort of forwards[portConfig.port]) {
										portL.debug(`Forwarding message on to ${forwardPort}`);
										serialPorts[forwardPort].write(Uint8Array.from(buffer));
									}
								}
							} catch (error) {
								portL.error('Error decoding message', buffer, error);
								buffer = [];
								continue;
							}
							buffer = [];
						} else {
							buffer.push(data[i]);
						}
					}
				});

				break;
			}
			case 'AsciiProgram': {
				const portL = L.createLogger(':' + portConfig.port);

				let buffer = [];

				// Add to forwards
				for (let forwardPort of portConfig.ports) {
					if (!forwards[forwardPort]) {
						forwards[forwardPort] = [portConfig.port];
					} else {
						forwards[forwardPort].push(portConfig.port);
					}
				}

				port.on('data', (data) => {
					portL.debug('got data', data, 'forst', data[0]);
					for (let i = 0; i < data.length; i++) {
						if (data[i] == 0x0d) {
							// \r
							// Send on
							for (let forwardPort of portConfig.ports) {
								if (!serialPorts[forwardPort]) {
									portL.error(`No connection to port ${forwardPort} to forward to`);
									continue;
								}
								portL.debug(`Forwarding message on to ${forwardPort}`);
								serialPorts[forwardPort].write(Uint8Array.from(buffer));
							}

							buffer = [];
						} else {
							buffer.push(data[i]);
						}
					}
				});
			}
		}
	}

	L.log('map27-radio-connect running');

	return () => {
		ws.close();
		for (let port of serialPorts) {
			port.close();
		}
	};
}

export default startRadioConnect;
