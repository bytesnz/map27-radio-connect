import chalk from 'chalk';

const levels = ['ERROR', 'WARN', 'LOG', 'INFO', 'DEBUG'];

/*TODO let givenLevel = process.env.LOG_LEVEL;
if (givenLevel) {
	const num = Number(givenLevel);
	if (!isNaN(num)) {
		setLogLevel(num);
	} else {
		setLogLevel(givenLevel);
	}
}
*/

/**
 * @callback LogFunction
 * @param {any[]} ...message Message to log
 */

/**
 * @typedef {Object} Logger
 * @param {string} level Log level
 * @param {any[]} ...message Message to log
 * @property {LogFunction} error Log error message
 * @property {LogFunction} warn Log warn message
 * @property {LogFunction} log Log log message
 * @property {LogFunction} info Log info message
 * @property {LogFunction} debug Log debug message
 */

/**
 * Create a new logger
 *
 * @param {string} id ID for new logger
 * @param {string|number} logLevel Log level for logging to console
 *
 * @returns {Logger}
 */
const createLogger = (id, logLevel) => {
	if (typeof logLevel === 'string') {
		logLevel = levels.indexOf(logLevel);
	} else if (typeof logLevel !== 'number') {
		logLevel = levels.indexOf('LOG');
	}

	if (logLevel === -1) {
		throw new Error('Unknown log level');
	}

	const setLogLevel = (level) => {
		if (typeof level === 'number') {
			logLevel = Math.max(0, Math.min(level, levels.length - 1));
		} else {
			level = levels.indexOf(level);
			if (level !== -1) {
				logLevel = level;
			}
		}

		logger.debug('Log level now', logLevel, levels[logLevel]);

		return levels[logLevel];
	};

	const error = (...message) => {
		if (logLevel >= levels.indexOf('ERROR')) {
			// eslint-disable-next-line no-console
			console.error(new Date().toISOString(), id, chalk.red('ERROR'), ...message);
		}
	};
	const warn = (...message) => {
		if (logLevel >= levels.indexOf('WARN')) {
			// eslint-disable-next-line no-console
			console.warn(new Date().toISOString(), id, chalk.yellow('WARN'), ...message);
		}
	};
	const log = (...message) => {
		if (logLevel >= levels.indexOf('LOG')) {
			// eslint-disable-next-line no-console
			console.log(new Date().toISOString(), id, chalk.green('LOG'), ...message);
		}
	};
	const info = (...message) => {
		if (logLevel >= levels.indexOf('INFO')) {
			// eslint-disable-next-line no-console
			console.info(new Date().toISOString(), id, chalk.magenta('INFO'), ...message);
		}
	};
	const debug = (...message) => {
		if (logLevel >= levels.indexOf('DEBUG')) {
			// eslint-disable-next-line no-console
			console.debug(new Date().toISOString(), id, chalk.cyan('DEBUG'), ...message);
		}
	};

	const willLog = (testLevel) => {
		if (typeof testLevel === 'string') {
			testLevel = levels.indexOf(testLevel);
		} else if (typeof testLevel !== 'number') {
			testLevel = levels.indexOf('LOG');
		}

		if (testLevel === -1) {
			throw new Error('Unknown log level');
		}

		return logLevel >= testLevel;
	};

	/* @type {Logger} */
	const logger = (level, ...message) => {
		switch (level) {
			case 'ERROR':
				return error(...message);
			case 'WARN':
				return warn(...message);
			case 'INFO':
				return info(...message);
			case 'DEBUG':
				return debug(...message);
			case 'LOG':
			default:
				return log(...message);
		}
	};

	Object.assign(logger, {
		setLogLevel,
		createLogger: (subId, newLogLevel) => createLogger(id + subId, newLogLevel || logLevel),
		error,
		warn,
		log,
		info,
		debug,
		willLog
	});

	Object.freeze(logger);

	return logger;
};

export default createLogger;
