import { Logger } from './lib/log.js';

interface Port {
	/// Address of port, eg. COM1, /dev/tty0 or STDIN
	port: string;
	/// Baud rate of port
	baud?: number;
}

interface ProgramPort extends Port {
	/// Type of protocol to use to communicate with radio
	protocol: 'AsciiProgram';
	/// Array of port addresses to forward messages to/from
	ports: Array<string>;
}

interface RadioPort extends Port {
	/// Type of protocol to use to communicate with radio
	protocol: 'taitAscii' | 'tait' | 'map27' | 'map27Ascii';
	/// Network type the radio is configured for
	networkType: 'dmr';
	/// Uses for connection
	use: Array<'dmrPolling'>;
}

export interface Config {
	/// WebSocket URL to on-location server
	server: string;
	/// Basic authentication username
	username?: string;
	/// Basic authentication password
	password?: string;
	// Log level to use
	logLevel?: 'ERROR' | 'WARN' | 'INFO' | 'DEBUG';
	/// Logger to use for logging instead of internal logger
	logger?: Logger;
	/// Port configurations
	ports: Array<RadioPort | ProgramPort>;
}
