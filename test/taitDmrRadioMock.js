import { stdin, argv0, argv, exit } from 'node:process';
import { SerialPort } from 'serialport';
import minimist from 'minimist';

const help = () => {
	console.error(`${argv0}

Mock a Tait DMR radio sending ASCII data to the radio

Usage:
${argv0} [-ha] [-p <port>] [-b <baudrate>]

  -p <port>      Port path to connect to (should be one end of a loopback)
                   default: /dev/ttyS11
  -b <baudrate>  Baudrate to use for the serial port
                   default: 19200
  -a             Auto respond to requests and basic messages
  -h, --help     Show this help
`);
};

const args = minimist(argv.slice(2));

if (args.h || args.help) {
	help();
	exit(0);
}

if (args.a) {
	console.log('Automating some responses');
}

const port = args.p || '/dev/ttyS11';
const baud = args.b || 19200;

let connected = false;

/**
 * Send data on serial port
 *
 * @param {string} data ASCII Hex data to send (without trailing \r)
 */
function sendMessageHex(data) {
	console.log('sending', data);
	serial.write(data + '\r');
	/*console.log('sending', (data.toString() + '\r').split('').map((x) => x.charCodeAt()));
  serial.write(Uint8Array.from(
    (data + '\r').split('').map((x) => x.charCodeAt())
  ));*/
}

stdin.on('data', (data) => {
	// Assume data will always end with line endings
	let last = data.length - 1;

	for (last; last >= 0; last--) {
		if (data[last] === 0x0d || data[last] === 0x0a) {
			continue;
		}
		break;
	}

	if (last <= 0) {
		return;
	}

	sendMessageHex(data.slice(0, last + 1));
});

const serial = new SerialPort({
	path: port,
	baudRate: baud
});

let sendCounter = 0;

let buffer = [];
serial.on('data', (data) => {
	for (let i = 0; i < data.length; i++) {
		if (data[i] == 0x0d) {
			const message = buffer.reduce((m, b) => m + String.fromCharCode(b), '');
			console.log('got message over serial', message);
			if (args.a) {
				if (message === 'b000') {
					//  Personality request
					sendMessageHex('b00405b304d04fdafe0000ef');
					sendMessageHex('b204b9ff');
				} else if (message.startsWith('84') && message.endsWith('05')) {
					// Tait DMR Poll 840405a40005
					sendMessageHex('b206baff'); // Off hook
					sendMessageHex('b207baff'); // Transmitting
					sendMessageHex('b206baff'); // Off hook
					switch (++sendCounter % 5) {
						case 0:
						case 1:
							// NACK out of reach or abandoned
							sendMessageHex('e0' + message.slice(2, 8) + '0004');
							break;
						case 4:
							sendMessageHex(
								// Call queued
								'd0' + message.slice(2, 8) + '0002'
							);
						/*eslint-disable-next-line no-fallthrough */
						default:
							sendMessageHex(
								// NEMA position
								'81' + message.slice(2, 8) + '003700000101000000291118e1ae310c3d141c2c'
							);
							break;
					}
					sendMessageHex('b204b9ff');
				}
			}
			buffer = [];
		} else {
			buffer.push(data[i]);
		}
	}
});

setInterval(() => {
	if (connected) {
		sendMessageHex('b204b9ff');
	}
	sendMessageHex('b54169c0ad');
}, 10000);

console.log(`Ready to send fake data to ${port}`);
console.log('Paste in any data and I will forward it on');
